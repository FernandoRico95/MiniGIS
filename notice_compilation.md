# Comment build le projet ?

## Prérequis : 

Avoir le projet en local (git clone).

Ouvrir un Terminal.

Se placer dans le dossier MiniGIS/MiniGIS :
`cd MiniGIS/MiniGIS`

Creer une image adaptée à la compilation d'un projet C++ :
`make build-docker-deps-image`

*Attention ! Cette opération est assez longue mais n'est à éxecuter **QU'UNE SEULE FOIS**.*


## Compilation : créer une build

Se placer dans le dossier MiniGIS/MiniGIS :
`cd MiniGIS/MiniGIS`

Lancer la compilation :
`make`


## Lancer l'éxecutable de test

Se placer dans le dossier MiniGIS/MiniGIS :
`cd MiniGIS/MiniGIS`

Puis dans le dossier de test :
`cd build/test`

Lancer l'éxecutable de test :
`./test_MiniGIS`


## Lancer l'application

Se placer dans le dossier MiniGIS/MiniGIS :
`cd MiniGIS/MiniGIS`

Puis dans le dossier de build :
`cd build`

Lancer l'éxecutable :
`./MiniGIS`

## Sources

https://gitlab.com/MokhtarG/minisig

https://ddanilov.me/dockerized-cpp-build

https://github.com/f-squirrel/dockerized_cpp/blob/master/Makefile