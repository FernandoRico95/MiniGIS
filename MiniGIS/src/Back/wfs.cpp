#include "../../include/Back/wfs.h"
#include "../../include/Back/vectorGeometry.h"

#include <iostream>


Wfs::Wfs(std::string path, std::string layerName):Layer(path.c_str(), layerName.c_str())
{

    GDALAllRegister();

    VectorGeometry vg;

    setDataset(fetchWFS(path));

    // Initialisation
    Feature geometry;
    foundLayerNames();
//    std::vector<std::string> l = {"FOR_PUBL_FR"};

//    computeGeometries(l);
}

Wfs::Wfs(std::string path):Layer(path.c_str(), "")
{

    GDALAllRegister();

    VectorGeometry vg;

    setDataset(fetchWFS(path));

    // Initialisation
    Feature geometry;
    foundLayerNames();
}

Wfs::~Wfs(){

}


int Wfs::GetProjection(std::string layerName){

     OGRLayer * layer = getDataset()->GetLayerByName(layerName.c_str());
     layer -> ResetReading();

     OGRGeometry * geom = layer->GetNextFeature()->GetGeometryRef();

     int scr = geom->getSpatialReference()->GetEPSGGeogCS();

     return scr;
}


GDALDataset* Wfs::fetchWFS(std::string filePath){
    std::string newFilePath = "WFS:"+filePath;

    GDALDataset* dataset = (GDALDataset*) GDALOpenEx(newFilePath.c_str(), GDAL_OF_VECTOR, NULL, NULL, NULL );

    if (dataset == NULL)
        throw std::invalid_argument("AddPositiveIntegers arguments must be positive");

    return dataset;
}


void Wfs::foundLayerNames(){
    for (int i = 0; i < getDataset()->GetLayerCount(); i++){
        OGRLayer * layer = getDataset()->GetLayer(i);
        pushLayerNames(layer->GetName());
    }
}


void Wfs::computeGeometries(std::vector<std::string> layerNames){

    std::vector<glm::vec3> geometries;
    Type typeGeom;
    Feature feature;
    std::vector<Feature> Listfeatures;

    for (auto &name : layerNames) // access by reference to avoid copying
    {
        OGRLayer *poLayer = getDataset()->GetLayerByName(name.c_str());



        OGRFeatureDefn *poFDefn = poLayer->GetLayerDefn();

        //poLayer->ResetReading();
        OGRFeature *poFeature;

        while( (poFeature = poLayer->GetNextFeature()) != NULL )
        {
            OGRGeometry *geometry = poFeature->GetGeometryRef();

            if( geometry != NULL )
            {
                OGRwkbGeometryType type = wkbFlatten(geometry->getGeometryType());
                std::vector<glm::vec3> geometries;

                switch(type)
                {
                   case wkbPoint:
                   {
                       geometries = this->vg.calculatePointVertex(geometry);
                       typeGeom = Type::POINT;

                       break;
                   }
                   case wkbMultiPoint:
                   {
                        //Get point data
                        OGRMultiPoint* multiPoint = (OGRMultiPoint*) geometry;
                        OGRGeometry* geompoint = multiPoint->getGeometryRef(0);

                        geometries = this->vg.calculatePointVertex(geompoint);
                        typeGeom = Type::POINT;

                        break;
                   }
                   case wkbLineString:
                   {
                        typeGeom = Type::LINE;
                        geometries = this->vg.calculateLineVertex(geometry);
                        std::cout<<"wkbLineString";

                        break;
                   }
                   case wkbMultiLineString:
                   {
                       //Get number of geometry (ex nb of lines)
                       OGRMultiLineString* multiline = (OGRMultiLineString*) geometry;

                       int nbGeometry = multiline->getNumGeometries();

                       for(int k = 0; k < nbGeometry; k++){
                           OGRGeometry* geomLine = multiline->getGeometryRef(k);
                           geometries = this->vg.calculateLineVertex(geomLine);
                       }

                       typeGeom = Type::LINE;

                       break;
                    }
                    case wkbPolygon:
                    {
                       geometries = this->vg.calculatePolygonVertex(geometry);
                       typeGeom = Type::POLYGON;


                       break;
                    }
                    case wkbMultiPolygon:
                    {
                        //Get number of geometry (ex nb of polygons)
                        OGRMultiPolygon* multipolygon = (OGRMultiPolygon*) geometry;

                        int nbGeometry = multipolygon->getNumGeometries();

                        for(int k = 0; k < nbGeometry; k++){
                            OGRGeometry* geomPolygon = multipolygon->getGeometryRef(k);
                            geometries = this->vg.calculatePolygonVertex(geomPolygon);
                        }

                        typeGeom = Type::POLYGON;

                        break;
                    }
                    default:
                        std::cout <<"aucun des types traités"<<std::endl;
               }
                feature.geometries = geometries;
                feature.type = typeGeom;
                Listfeatures.push_back(feature);
            }
       }

       this->setLayerFeature(Listfeatures);
    }

}

std::vector<std::string> Wfs::getAttributesID(long IID)
{
    GDALAllRegister();

   std::vector<std::string> result;


    OGRFeature *poFeature;

    Type typeGeom;

    GDALDataset *poDS = static_cast<GDALDataset*>(GDALDataset::Open( path.c_str(), GDAL_OF_VECTOR));
    OGRLayer  *poLayer = poDS->GetLayerByName( name.c_str() );

    while( (poFeature = poLayer->GetNextFeature()) != NULL ){
         if( poLayer != NULL)
         {

             OGRFeatureDefn *poFDefn = poLayer->GetLayerDefn();


             if(IID==poFeature->GetFID()){

                 for( int iField = 0; iField < poFDefn->GetFieldCount(); iField++ )
                 {

                     const char* values = poFeature->GetFieldAsString(iField);

                     result.push_back(std::string(values));

                }
             }
         }
    }

    return result;
}

vector<float> Wfs::getBoundingBoxFeature(uint index)
{
    LayerFeatures LF = getLayerFeatures();
    Feature Feat = LF.features.at(index);
    vector<glm::vec3>geometries = Feat.geometries;
    float MinX = geometries.at(0).x;
    float MinY = geometries.at(0).y;
    float MaxX = geometries.at(0).x;
    float MaxY = geometries.at(0).y;
    for (int i=0; i<geometries.size(); i++)
    {
        if (geometries.at(i).x<MinX){MinX = geometries.at(i).x;}
        if (geometries.at(i).y<MinY){MinY = geometries.at(i).y;}
        if (geometries.at(i).x>MaxX){MaxX = geometries.at(i).x;}
        if (geometries.at(i).y>MaxY){MaxY = geometries.at(i).y;}
    }
    return vector<float>{MinX, MaxX, MinY, MaxY};
}
vector<float> Wfs::getBoundingBoxLayer()
{
    std::vector<Feature> LFs = this->getLayerFeatures().features;

    Feature Feat = LFs.at(0);

    float MinX = (LFs.at(0).geometries.at(0)).x;
    float MinY = (LFs.at(0).geometries.at(0)).y;
    float MaxX = (LFs.at(0).geometries.at(0)).x;
    float MaxY = (LFs.at(0).geometries.at(0)).y;
    for (uint index=0; index<LFs.size() ; index++)
    {
        Feature Feat = LFs.at(index);
        vector<glm::vec3>geometries = Feat.geometries;
        for (int i=0; i<geometries.size(); i++)
        {
            if ((LFs.at(index).geometries.at(i)).x<MinX){MinX = (LFs.at(index).geometries.at(i)).x;}
            if ((LFs.at(index).geometries.at(i)).y<MinY){MinY = (LFs.at(index).geometries.at(i)).y;}
            if ((LFs.at(index).geometries.at(i)).x>MaxX){MaxX = (LFs.at(index).geometries.at(i)).x;}
            if ((LFs.at(index).geometries.at(i)).y>MaxY){MaxY = (LFs.at(index).geometries.at(i)).y;}
        }

    }
    return vector<float>{MinX, MaxX, MinY, MaxY};
}
