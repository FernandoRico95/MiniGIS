#include "../../include/Back/layerGML.h"


LayerGML::LayerGML(std::string path, std::string layerName ):Layer(path, layerName)
{
    computeGeometries();
    try {
        citygml::ParserParams params;
        std::shared_ptr<const citygml::CityModel> city = citygml::load(path, params);
        const citygml::Envelope & env = city->getEnvelope();
        srs = std::stoi(env.srsName().substr(5));
    } catch(std::exception e) {
        std::cout << "srs isn't defined in the gml file\n";
        srs = 0;
    }
}

void LayerGML::computeGeometries()
{
    /* this method is browsing the entire file to instantiate the LayerFeature object
     * LayerFeature contains every geometries in the file, it is a vector of Feature objects
     *
     * the file architecture is
     * file (file has some general metadatas like the producer)
     *   -> object (equivalent to a feature, it has some attributes and a type (building, bridge...))
     *        -> geometry (composed of polygons, but also lines... not considered here)
     *             -> polygon (polygon object)
     *                  -> vertice (coordinates)
     *
     * The LayerFeature is a vector of Features,
     * Feature Object has the type of the geometry and the list of its coordinates
     */

    std::vector<Feature> Listfeatures;

    citygml::ParserParams params;

    // accessing the gml file
    std::shared_ptr<const citygml::CityModel> city = citygml::load(path, params);
    const citygml::ConstCityObjects& roots = city->getRootCityObjects();

    // browsing every object contained in the file
    for ( unsigned int idxObject = 0; idxObject < roots.size(); idxObject++ ) {
        const citygml::CityObject* object = roots[idxObject]; // accessing object
        std::vector<Feature> objectListFeatures = LayerGML::analyzeObject(object);
        Listfeatures.insert(Listfeatures.end(), objectListFeatures.begin(), objectListFeatures.end());
    }
    setLayerFeature(Listfeatures);
}


std::vector<Feature> LayerGML::analyzeObject(const citygml::CityObject* object){

    std::vector<Feature> objectListFeatures;

    // browsing every geometries contained in the object
    for ( unsigned int idxGeom = 0; idxGeom < object->getGeometriesCount(); idxGeom++ ) {
        // initialising
        Feature feature;
        std::vector<glm::vec3> geometries; // initialialising object's geometries
        const citygml::Geometry geometry = object->getGeometry(idxGeom); // accessing the geometry

        // browsing every polygon contained in the geometry
        for (int idxPoly=0; idxPoly<geometry.getPolygonsCount();idxPoly++) {

            std::shared_ptr<const citygml::Polygon> polygon = geometry.getPolygon(idxPoly); // accessing the polygon
            std::vector<TVec3d> vertices = polygon->getVertices(); // accessing vertices list

            // browsing every vertice contained in the polygon
            for(int idxVertice=0; idxVertice<vertices.size();idxVertice++) {
                TVec3d vec3d = vertices.at(idxVertice); // accessing vertice
                geometries.push_back(glm::vec3(vec3d.x,vec3d.y,vec3d.z));
            }
        }
        feature.geometries = geometries;
        feature.type = Type::POLYGON;
        objectListFeatures.push_back(feature);
    }

    for (int idxChildObject=0 ; idxChildObject<object->getChildCityObjectsCount() ; idxChildObject++)
    {
        const citygml::CityObject& childObject = object->getChildCityObject(idxChildObject);
        std::vector<Feature> childObjectListFeatures = LayerGML::analyzeObject(&childObject);
        objectListFeatures.insert(objectListFeatures.end(), childObjectListFeatures.begin(), childObjectListFeatures.end());
    }

    return objectListFeatures;
}

void LayerGML::getAttributes()
{

    // initialise
    std::vector<std::vector<string>> result;
    citygml::ParserParams params;

    // accessing the gml file
    std::shared_ptr<const citygml::CityModel> city = citygml::load(path, params);
    const citygml::ConstCityObjects& roots = city->getRootCityObjects();

    // browsing every object contained in the file
    for ( unsigned int idxObject = 0; idxObject < roots.size(); idxObject++ ) {
        result.push_back(std::vector<string>{});
        const citygml::CityObject* object = roots[idxObject]; // accessing object
        std::cout << "type object => " << object->getTypeAsString() << endl;
        citygml::AttributesMap attributes = object->getAttributes();
        for(auto it=attributes.begin() ; it!=attributes.end() ; ++it)
        {
            std::cout << it->first << " => " << it->second << endl;
            //result[result.size()-1].push_back(string(it->first));
        }
    }
}

vector<float> LayerGML::getBoundingBox()
{
    /* This method returns the bounding box of the GML Layer
     * It doesn't use the getEnvelope method of citygml::CityModel object
     * because it wouldn't take in count the changing srs
     *
     * The returned vector is of size 6, it contained the min and max coordinates as
     * {min x,max x,min y, max y,min z, max z}
     */

    std::vector<Feature> LFs = this->getLayerFeatures().features;

    Feature Feat = LFs.at(0);

    float MinX = (LFs.at(0).geometries.at(0)).x;
    float MinY = (LFs.at(0).geometries.at(0)).y;
    float MinZ = (LFs.at(0).geometries.at(0)).z;
    float MaxX = (LFs.at(0).geometries.at(0)).x;
    float MaxY = (LFs.at(0).geometries.at(0)).y;
    float MaxZ = (LFs.at(0).geometries.at(0)).z;
    for (uint index=0; index<LFs.size() ; index++)
    {
        Feature Feat = LFs.at(index);
        vector<glm::vec3>geometries = Feat.geometries;
        for (int i=0; i<geometries.size(); i++)
        {
            if ((LFs.at(index).geometries.at(i)).x<MinX){MinX = (LFs.at(index).geometries.at(i)).x;}
            if ((LFs.at(index).geometries.at(i)).y<MinY){MinY = (LFs.at(index).geometries.at(i)).y;}
            if ((LFs.at(index).geometries.at(i)).z<MinZ){MinZ = (LFs.at(index).geometries.at(i)).z;}
            if ((LFs.at(index).geometries.at(i)).x>MaxX){MaxX = (LFs.at(index).geometries.at(i)).x;}
            if ((LFs.at(index).geometries.at(i)).y>MaxY){MaxY = (LFs.at(index).geometries.at(i)).y;}
            if ((LFs.at(index).geometries.at(i)).z>MaxZ){MaxZ = (LFs.at(index).geometries.at(i)).z;}
        }
    }
    return vector<float>{MinX, MaxX, MinY, MaxY, MinZ, MaxZ};
}

void LayerGML::changeSRS(int targetSRS){
    layerFeatures.setSRS(srs);
    layerFeatures.changeSRS(targetSRS);
}
