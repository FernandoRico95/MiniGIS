#include "../../include/Back/layerraster.h"
#include <iostream>
#include "cpl_conv.h" // for CPLMalloc()


LayerRaster::LayerRaster(std::string path, std::string layerName):Layer(path.c_str(), layerName.c_str())
{

}


void LayerRaster::openGeotiff(std::string path)
{
    /* Connexion to GeoTIFF using GDAL */

    //std::cout<<path;
    GDALAllRegister();
    this->geotiffData = (GDALDataset *) GDALOpen(path.c_str(), GA_ReadOnly );
}

int* LayerRaster::getGeotiffSize()
{
    if( this->geotiffData == NULL )
    {
        std::cout<< "Fail to open GeoTIFF" <<std::endl;
        return NULL;
    }

    int* dataArray = new int[3];
    // Dimensions
    dataArray[0] = this->geotiffData->GetRasterXSize();
    dataArray[1] = this->geotiffData->GetRasterYSize();
    // Nb of Bands
    dataArray[2] = this->geotiffData->GetRasterCount();

    return dataArray;
}


float* LayerRaster::getGeotiffOrigin()
{
    double adfGeoTransform[6];

    if(this->geotiffData->GetGeoTransform(adfGeoTransform) != CE_None )
    {
        return NULL;
    }

    float* dataArray = new float[2];
    // Origin location
    dataArray[0] = adfGeoTransform[0];
    dataArray[1] = adfGeoTransform[3];

    return dataArray;
}


float* LayerRaster::getPixelSize()
{
    double adfGeoTransform[6];

    if(this->geotiffData->GetGeoTransform(adfGeoTransform) != CE_None )
    {
        return NULL;
    }

    float* dataArray = new float[2];
    // Pixel size
    dataArray[0] = adfGeoTransform[1];
    dataArray[1] = adfGeoTransform[5];

    return dataArray;
}


const char* LayerRaster::getGeotiffProjection()
{
    if( this->geotiffData == NULL )
    {
        std::cout<< "Fail to open the GeoTIFF" <<std::endl;
        return 0;
    }
    const char* projection = this->geotiffData->GetProjectionRef() ;

    return projection;
}

vector<float> LayerRaster::getRasterBox()
{
    double adfGeoTransform[6];

    if(this->geotiffData->GetGeoTransform(adfGeoTransform) != CE_None )
    {
        return vector<float>{};
    }

    float Xmin = adfGeoTransform[0];
    float Xmax = Xmin + widthTIFF*adfGeoTransform[1];
    float Ymax = adfGeoTransform[3];
    float Ymin = Ymax + heightTIFF*adfGeoTransform[5];

    return vector<float>{Xmin, Xmax, Ymin, Ymax};
}

