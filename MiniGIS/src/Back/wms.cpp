#include<iostream>
#include<vector>

#include "../../include/Back/wms.h"

using namespace std;

WMS::WMS(std::string url, string LayerName) : Layer(url, LayerName)
{
    openWMS(url);

    // initialisation of width/height/bands
    int * WMSSize = getWMSSize();
    this->widthWMS = WMSSize[0];
    this->heightWMS = WMSSize[1];
    this->numberOfBands = WMSSize[2];

    std::cout<<this->widthWMS<<"\n";
    std::cout<<this->heightWMS<<"\n";
    std::cout<<this->numberOfBands<<"\n";

    this->Xo = 0;//getGeotiffOrigin()[0];
    this->Yo = 0;//getGeotiffOrigin()[1];
    this->Xsize = 0.001;//getPixelSize()[0];
    this->Ysize = 0.001;//getPixelSize()[0];

    fetchWMS(url);
    createIndexTable(url);
}

WMS::~WMS(){
}

void WMS::openWMS(std::string url){
    GDALAllRegister();
    std::cout<<url;

    this->WMSData = (GDALDataset*)GDALOpen(url.c_str(), GA_ReadOnly);

    if( WMSData == NULL )
    {
       std::cout << "Invalid URL (in wms.cpp)" << std::endl;

        GDALClose( WMSData );
        exit( 1 );
    }
}

int* WMS::getWMSSize()
{
    if( this->WMSData == NULL )
    {
        std::cout<< "Fail to open GeoTIFF" <<std::endl;
        return NULL;
    }

    int* dataArray = new int[3];
    // Dimensions
    dataArray[0] = this->WMSData->GetRasterXSize();
    dataArray[1] = this->WMSData->GetRasterYSize();
    // Nb of Bands
    dataArray[2] = this->WMSData->GetRasterCount();

    return dataArray;
}

float* WMS::getWMSOrigin()
{
    double adfGeoTransform[6];

    if(this->WMSData->GetGeoTransform(adfGeoTransform) != CE_None )
    {
        return NULL;
    }

    float* dataArray = new float[2];
    // Origin location
    dataArray[0] = adfGeoTransform[0];
    dataArray[1] = adfGeoTransform[3];

    return dataArray;
}

float* WMS::getPixelSize()
{
    double adfGeoTransform[6];

    if(this->WMSData->GetGeoTransform(adfGeoTransform) != CE_None )
    {
        return NULL;
    }

    float* dataArray = new float[2];
    // Pixel size
    dataArray[0] = adfGeoTransform[1];
    dataArray[1] = adfGeoTransform[5];

    return dataArray;
}

const char* WMS::getWMSProjection()
{
    if( this->WMSData == NULL )
    {
        std::cout<< "Fail to open the GeoTIFF" <<std::endl;
        return 0;
    }
    const char* projection = this->WMSData->GetProjectionRef() ;

    return projection;
}

void WMS::fetchWMS(std::string path){
    GDALAllRegister();
    this->WMSData = (GDALDataset *) GDALOpen(path.c_str(), GA_ReadOnly );
    // result defines a vect of GeoTIFF pixel data
    std::vector<glm::vec3> result;

//    std::vector<std::vector<float>> outR = std::vector<std::vector<float>>(widthTIFF, std::vector<float>(heightTIFF, 0));
//    std::vector<std::vector<float>> outG = std::vector<std::vector<float>>(widthTIFF, std::vector<float>(heightTIFF, 0));
//    std::vector<std::vector<float>> outB = std::vector<std::vector<float>>(widthTIFF, std::vector<float>(heightTIFF, 0));
      std::vector<std::vector<float>> out = std::vector<std::vector<float>>(widthWMS, std::vector<float>(heightWMS, 0));
//    std::vector<std::vector<std::vector<float>>> out = {outR, outG, outB};

    for(int k = 1; k <= this->numberOfBands; k++){
        GDALRasterBand  *rasterBand = this->WMSData->GetRasterBand(k);
        float *pafScanline;
        pafScanline = (float *) CPLMalloc(sizeof(float)*this->widthWMS*this->heightWMS);
        // RasterIO function browse the raster bands and store the data in the pafscanline
        if(rasterBand->RasterIO(GF_Read, 0, 0, this->widthWMS, this->heightWMS, pafScanline, this->widthWMS, this->heightWMS, GDT_Float32, 0, 0) == CE_None)
        {
            // Construct a height map based on the xres and yres for each group of four dots
            for (int i = 0; i < this->heightWMS; i++)
            {
                for (int j = 0; j < this->widthWMS; j++)
                {
                    if (pafScanline[i*this->widthWMS + j] > 0) {
                        out[j][i] = pafScanline[(i)*widthWMS + j];
                    }
                    else
                        out[j][i] = 0;
                }
            }
        } else {
            std::cout<<"Error: can't read raster bands"<<std::endl;
        }
        CPLFree(pafScanline);
    }
    // Time to construct a height map based on the xres and yres for each group of four dots
    for (int i = 0; i < out.size() - 1; i++)
    {
        for (int j = 0; j < out[i].size() - 1; j++)
        {
            result.push_back(glm::vec3( Xo + (float)i*Xsize        , Yo + (float)j*Ysize      , 0.0 ) );
            result.push_back(glm::vec3( Xo + (float)(i + 1)*Xsize  , Yo + (float)j*Ysize      , 0.0 ) );
            result.push_back(glm::vec3( Xo + (float)i*Xsize        , Yo + (float)(j + 1)*Ysize, 0.0 ) );

            result.push_back(glm::vec3( Xo + (float)(i + 1)*Xsize  , Yo + (float)j*Ysize      , 0.0 ) );
            result.push_back(glm::vec3( Xo + (float)i*Xsize        , Yo + (float)(j + 1)*Ysize, 0.0 ) );
            result.push_back(glm::vec3( Xo + (float)(i + 1)*Xsize  , Yo + (float)(j + 1)*Ysize, 0.0 ) );
            //result.push_back((glm::vec3( out[0][j][i], out[1][j][i], out[2][j][i])));
        }
    }
    this->dataWMS = result;


        // Get raster image size
        int rows = heightWMS;
        int cols = widthWMS;
        int channels = numberOfBands;

    QImage imData = QImage(cols, rows, QImage::QImage::Format_RGBA8888);
     imData.fill(QColor(255, 255, 255, 255));
     // Bands start at 1
     for (int i = 1; i <= numberOfBands; ++i)
     {
         GDALRasterBand *band = this->WMSData->GetRasterBand(i);
         switch(band->GetColorInterpretation())
         {
         case GCI_RedBand:
             band->RasterIO(GF_Read, 0, 0, cols, rows, imData.bits(),
                            cols, rows, GDT_Byte, 4, 0);
             break;
         case GCI_GreenBand:
             band->RasterIO(GF_Read, 0, 0, cols, rows, imData.bits() + 1,
                            cols, rows, GDT_Byte, 4, 0);
             break;
         case GCI_BlueBand:
             band->RasterIO(GF_Read, 0, 0, cols, rows, imData.bits() + 2,
                            cols, rows, GDT_Byte, 4, 0);
             break;
         }
     }

     GDALClose(this->WMSData);
    this->qtimage = imData;
}

void WMS::createIndexTable(std::string path)
{
    std::vector<glm::vec2> indices;

    for(int i = 0; i < widthWMS-1; i++)       // for each row a.k.a. each strip
    {
        for(int j = 0; j < heightWMS-1; j++)      // for each column
        {
            //first triangle

            indices.push_back(glm::vec2((float)i / (float)widthWMS, (float)j / (float)heightWMS));
            indices.push_back(glm::vec2((float)(i + 1) / (float)widthWMS, (float)j / (float)heightWMS));
            indices.push_back(glm::vec2((float)i / (float)widthWMS, (float)(j + 1) / (float)heightWMS));

            //second triangle

            indices.push_back(glm::vec2((float)(i + 1) / (float)widthWMS, (float)j / (float)heightWMS));
            indices.push_back(glm::vec2((float)i / (float)widthWMS, (float)(j + 1) / (float)heightWMS));
            indices.push_back(glm::vec2((float)(i + 1) / (float)widthWMS, (float)(j + 1) / (float)heightWMS));

        }
    }
    this->indices = indices;
}



//void WMS::createIndexTable(const char* url)
//{
//    GDALAllRegister();
//    WMSData = static_cast<GDALDataset*>(
//    (GDALDataset*)GDALOpenEx(url, GDAL_OF_RASTER,NULL,NULL,NULL ));
//        // IndexTable is a table that'll store indexBuffer data associated to dataTIFF to create triangles
//    std::vector<unsigned int> result;
//    // the loop will store each time 6 values of IndexTable for 2 triangles
//    for (int i = 0; i < this->heightWMS - 1; i++) {
//            for (int j = 0; j < this->widthWMS; j++) {
//                // first triangle
//                // point in line i and column j, 6 means each time the values restart from the 7th value and k if there are different raster bands
//                result.push_back( i * this->widthWMS + j );
//                // point in line i+1 and column j
//                result.push_back( (i + 1) * this->widthWMS + j );
//                // point in line i+1 and column j+1
//                result.push_back( (i + 1) * this->widthWMS + j + 1);

//                // second triangle
//                // point in line i and column j
//                result.push_back( i * this->widthWMS + j );
//                // point in line i and column j+1
//                result.push_back( i * this->widthWMS + j + 1 );
//                // point in line i+1 and column j+1
//                result.push_back( (i + 1) * this->widthWMS + j + 1 );
//            }
//        }
//    this->indexTable = result;

//    }

//void WMS::fetchWMS(const char* url){
//    GDALAllRegister();

//    WMSData = static_cast<GDALDataset*>(
//    (GDALDataset*)GDALOpenEx(url, GDAL_OF_RASTER,NULL,NULL,NULL ));
//        // result defines a vect of GeoTIFF pixel data
//    std::vector<glm::vec3> result;
//    std::vector<std::vector<float>> out = std::vector<std::vector<float>>(widthWMS, std::vector<float>(heightWMS, 0));
//    for(int k = 1; k <= this->numberOfBands; k++){
//        GDALRasterBand  *rasterBand = this->WMSData->GetRasterBand(k);
//        float *pafScanline;
//        pafScanline = (float *) CPLMalloc(sizeof(float)*this->widthWMS*this->heightWMS);
//        // RasterIO function browse the raster bands and store the data in the pafscanline
//        if(rasterBand->RasterIO(GF_Read, 0, 0, this->widthWMS, this->heightWMS, pafScanline, this->widthWMS, this->heightWMS, GDT_Float32, 0, 0) == CE_None)
//        {
//            // Construct a height map based on the xres and yres for each group of four dots
//            for (int i = 0; i < this->heightWMS; i++)
//            {
//                for (int j = 0; j < this->widthWMS; j++)
//                {
//                    if (pafScanline[i*this->widthWMS + j] > 0) {
//                        out[j][i] = pafScanline[(i)*widthWMS + j];
//                    }
//                    else
//                        out[j][i] = 0;
//                }
//            }
//        } else {
//            std::cout<<"Error: can't read raster bands"<<std::endl;
//        }
//        CPLFree(pafScanline);
//    }
//    // Time to construct a height map based on the xres and yres for each group of four dots
//    for (int i = 0; i < out.size() - 1; i++)
//    {
//        for (int j = 0; j < out[i].size() - 1; j++)
//        {
//            result.push_back(glm::vec3( i , j , out[i][j] ) );
//            result.push_back(glm::vec3(  (i + 1) , j , out[i+1][j] ) );
//            result.push_back(glm::vec3( i , (j + 1) , out[i][j+1] ) );
//            result.push_back(glm::vec3(  (i + 1) , (j+1) , out[i+1][j+1] ) );
//            result.push_back(glm::vec3( (i + 1) , j   , out[i+1][j] ) );
//            result.push_back(glm::vec3( i , (j + 1) , out[i][j+1] ) );
//        }
//        }

//    this->dataWMS = result;

//    // Load image
//        GDALDataset* dataset = static_cast<GDALDataset*>(GDALOpen(QString::fromStdString(path).toLocal8Bit().data(), GA_ReadOnly));

//        // Get raster image size
//        int rows = dataset->GetRasterYSize();
//        int cols = dataset->GetRasterXSize();
//        int channels = dataset->GetRasterCount();

//    std::vector<std::vector<uchar>> bandData(channels);
//    for (auto& mat : bandData)
//    {
//        mat.resize(size_t(rows * cols));
//    }
//    std::vector<uchar> outputImage(size_t(4 * rows * cols));

//    // Iterate over each channel
//    for (int i = 1; i <= numberOfBands; ++i)
//    {
//        // Fetch the band
//        GDALRasterBand* band = WMSData->GetRasterBand(i);

//        // Read the data
//        band->RasterIO(GF_Read, 0, 0, cols, rows, bandData[size_t(i - 1)].data(),
//                cols, rows, GDT_Byte, 0, 0);
//    }

//    for (size_t i = 0, j = 0; i < outputImage.size(); i += 3, j += 1)
//    {
//        outputImage[i] = bandData[0][j];
//        outputImage[i + 1] = bandData[1][j];
//        outputImage[i + 2] = bandData[2][j];
//        //outputImage[i + 3] = bandData[3][j];
//    }

//    // Create the QImage (or even a QPixmap suitable for displaying teh image
//    QImage qtImage(outputImage.data(), cols, rows, QImage::Format_RGBA8888);

//    this->qtimage = qtImage;

//}
