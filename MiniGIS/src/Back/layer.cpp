#include "../../include/Back/layer.h"

Layer::Layer()
{
    projection = "2154";
    hidden = false;
    selected = false;
}

Layer::Layer(std::string _path, std::string _layerName)
{
    path = _path;
    name = _layerName;
    projection = "2154";
    hidden = false;
    selected = false;
}
