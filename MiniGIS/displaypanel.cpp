#include "displaypanel.h"
#include <iostream>
#include "glm/gtx/transform.hpp"
#include "displaypanel.h"
#include "../include/Back/wfs.h"





DisplayPanel::DisplayPanel(QWidget *parent) : QOpenGLWidget(parent){
    QOpenGLWidget::setFocusPolicy(Qt::StrongFocus);
    xRot = 0.0;
    yRot = 0.0;
    zRot = 0.0;
    projMatrix = glm::mat4(1);
    viewMatrix = glm::mat4(1);
}

DisplayPanel::~DisplayPanel() {
}

void DisplayPanel::changeXdMode(bool state){
    if (state == 0){ //2d model

        xdMode = 0;
        xRot = 0;
        yRot = 0;
        zRot = 0;
    }
    else{ //3d model
        xdMode = 1;
    }
}

static void qNormalizeAngle(float &angle){
    while (angle < 0)
        angle += 360 * 16;
    while (angle > 360 * 16)
        angle -= 360 * 16;
}

void DisplayPanel::setXRotation(float angle){
    qNormalizeAngle(angle);
    if(angle != xRot){
        xRot = angle;
        emit XRotationChanged(angle);
        update();
    }
}

void DisplayPanel::setYRotation(float angle){
    qNormalizeAngle(angle);
    if(angle != yRot){
        yRot = angle;
        emit YRotationChanged(angle);
        update();
    }
}

void DisplayPanel::setZRotation(float angle){
    qNormalizeAngle(angle);
    if(angle != zRot){
        zRot = angle;
        emit ZRotationChanged(angle);
        update();
    }
}

void DisplayPanel::initializeGL() {
    glClearColor(1.0f,1.0f,1.0f,1.0f);
    glClearColor(0,0,0,1);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
}

void DisplayPanel::paintGL() {
    //Point size
    glColor3f(1.0, 1.0, 1.0);
    glPointSize(5);

    // make points round
    glEnable(GL_POINT_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // change line width
    glLineWidth(2);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    //initalized scene properties
    glScaled(zoom, zoom, 1.0f);

    glTranslatef(xDep,yDep,-80);
    glRotatef(xRot / 16.0, 1.0, 0.0, 0.0);
    glRotatef(yRot / 16.0, 0.0, 1.0, 0.0);
    glRotatef(zRot / 16.0, 0.0, 0.0, 1.0);

    //rotation coordinate
    viewMatrix = glm::rotate(glm::rotate(glm::rotate(glm::translate(glm::mat4(1), glm::vec3(xDep, yDep, zoom)), xRot/16.0f, glm::vec3(1,0,0)), yRot/16.0f, glm::vec3(0,1,0)), zRot/16.0f, glm::vec3(0,0,1));
/*
    // Prepare texture
    QImage image = QImage();
    image.load("/home/fernando/Downloads/chat.jpg");

    QOpenGLTexture *texture = new QOpenGLTexture(image.mirrored());
    // Render with texture
    texture->bind();*/
    draw();
    update();
    glDisable(GL_TEXTURE_2D);
}

void DisplayPanel::resizeGL(int w, int h) {
    int side = qMin(w,h);
    glViewport( (w-side)/2 , (h-side)/2 ,side,side);//specifies the affine transformation of x and y from normalized device coordinates to window coordinates

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(45, (float)w/h, 0.01, 100.0); // set up a perspective projection matrix
    projMatrix = glm::perspective(45.0f*(float)M_PI/180, (float)w/h, 0.01f, 100.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //gluLookAt(0,0,5,0,0,0,0,1,0);
    gluOrtho2D(w/2, w, h/2, h);
}

void DisplayPanel::setZoomplus(){ // set zoom + function
    if(zoom < 15000){
        zoom += zoom/5;
    }
}

void DisplayPanel::setZoomminus(){ //set zoom - function
    if(zoom > 1){
        zoom -= zoom/5;
    }
}

// zoom with mouse scroll function
void DisplayPanel::wheelEvent(QWheelEvent *event){
    int scroll = event->angleDelta().y();
    if(scroll > 0){
         setZoomplus();
        }

       else if(scroll < 0){
        setZoomminus();
            }
}

// listen mouse click for active pan fucntion
void DisplayPanel::mousePressEvent(QMouseEvent *event){
    //get cursor coordonate
    lastPos = event->pos();
}


// Pan scene function
void DisplayPanel::mouseMoveEvent(QMouseEvent *event){

    if (!(QApplication::keyboardModifiers() && Qt::ControlModifier)){
    // get pan x difference
    float dx = event->pos().x() - lastPos.x();
    // get pan y difference
    float dy = event->pos().y() - lastPos.y();
    //QCursor::setPos(mapToGlobal(lastPos));

    if(zoom > -100 && zoom < -75){
        ZDep = -3;}

    if(zoom > -75 && zoom < -50){
        ZDep = -4;}

    if(zoom > -50 && zoom < -25){
        ZDep = -5;}

    if(zoom > -25 && zoom < 0){
        ZDep = -6;}

    xDep += dx/(-zoom)*-1/9;
    yDep -= dy/(-zoom)*-1/9;
    lastPos = event->pos();
    }

    // if rotation
    else if (xdMode == 1){
        // new position
        float dx = event->pos().x() - lastPos.x() ;
        float dy = event->pos().y() - lastPos.y();

        // rotation on Y axis
        if (abs(dx) >=  abs(dy)){
            setXRotation(xRot+4*dy);
            setYRotation(yRot+4*dx);
        }
        // rotation on X axis
        else{
            setXRotation(xRot+4*dy);
            setZRotation(zRot+4*dx);
        }
        lastPos = event->pos();
    }
}

void DisplayPanel::mouseReleaseEvent(QMouseEvent *event){
    int x_m = event->pos().x();
    int y_m = event->pos().y();
    int widgetWidth = this->size().width();
    int widgetHeight = this->size().height();
    glm::vec3 cam_pos(xDep, yDep, zoom);
    glm::vec3 cam_rot(xRot*M_PI/180.0f/16.0f, yRot*M_PI/180.0f/16.0f, zRot*M_PI/180.0f/16.0f);

    std::vector<uint>* selected_feat_ID = new std::vector<uint>();

    if (selected_object(x_m, y_m, widgetWidth, widgetHeight, cam_pos, cam_rot, selected_feat_ID)) {
        Object_feat_ID = *selected_feat_ID;
        getObjectClickedData();
        MainWindow * mw = MainWindow::getMainWinPtr();
        mw->getObjectinfo();
         cout<<"Object selected."<<endl;
    } else {
        cout<<"--- Object not selected."<<endl;
    }
    delete selected_feat_ID;

}

void DisplayPanel::setLayersPanel(LayersPanel* _layersPanel){
    layersPanel = _layersPanel;
}

void DisplayPanel::setProject(Project* _project){
    project = _project;
}

vector<float> middleCoord(vector<float> bbox){
    // min x, max x, min y, max y
    return {(bbox[1]+ bbox[0])/2, (bbox[3]+bbox[2])/2};
}

void DisplayPanel::centerLayer(uint id){
    //vector<uint> keys = project->extract_keys();
    //Layer lay = project->LayerByID(id);
    Format format = project->LayerByID(id)->format;
    if ((format == Format::GML) || (format == Format::SHP) || (format == Format::WFS))
    {
        LayerFeatures geoType= project->layerFeatures[id];
        vector<float> coord = middleCoord(geoType.bbox);
        xDep = 0-coord[0];
        yDep = 0-coord[1];
    }

    else if ((format == Format::MNT) || (format == Format::TIFF) || (format == Format::WMS)){
        vector<float> coord = middleCoord(project->layerRastersBBox.at(id));
        for (int i=0; i<coord.size(); i++){std::cout<<coord[i]<<std::endl;}
        xDep = 0-coord[0];
        yDep = 0-coord[1];
    }
}

void DisplayPanel::draw() {
    //QColor(Qt::red);

    vector<uint> keys = project->extract_keys();//project->layerFeatures.size();
    int nb_layers = keys.size();
//    std::cout<<"0"<<std::endl;
    //nb_layers = project->getLayers()->size();

    if(nb_layers !=0)
    {
//        std::cout<<"1"<<std::endl;
        for(int i=0; i<nb_layers; i++)
        {

             Layer layer = project->getLayers()->at(keys.at(i));

             if(layer.getHidden()==0)
             {
                 if(layer.format==Format::SHP)
                 {
                    LayerFeatures geoType= project->layerFeatures[keys.at(i)];
                    //std::cout<<geoType[1];
                    display(geoType);

//                    std::cout<<"SHP";
                 }
                 else if(layer.format==Format::GML)
                 {

                    LayerFeatures geoType= project->layerFeatures[keys.at(i)];
                    //std::cout<<"CityGML's Height : " << geoType.features.at(0).geometries.at(0)[2]<<endl;
                    display(geoType);

                    //std::cout<<"GML";
                 }

                 else if(layer.format==Format::TIFF)
                 {
                     std::vector<glm::vec3> datatiff= project->layerRastersData[keys.at(i)];
                     QImage img= project->layerRastersDataQimage[keys.at(i)];
                     std::vector<glm::vec2> indices = project->layerRastersIndices[keys.at(i)];
                     //QImage img = QImage("/home/fernando/Downloads/EO_Browser_images/2022-12-01-00:00_2022-12-01-23:59_Sentinel-2_L2A_True_color.jpg");
                     display2D(datatiff,indices,img);
//                     LayerRaster lr(layer.getPath(), layer.getName());
//                     std::cout<<layer.getName();
//                     std::vector<glm::vec3> datatiff = lr.dataTIFF;

//                     display(datatiff,layer.getPath());
                  }
                 else if(layer.format==Format::MNT)
                 {
                     std::vector<glm::vec3> datatiff= project->layerRastersData[keys.at(i)];
                     QImage img= project->layerRastersDataQimage[keys.at(i)];
                     std::vector<glm::vec2> indices = project->layerRastersIndices[keys.at(i)];
                     //QImage img = QImage("/home/fernando/Downloads/EO_Browser_images/2022-12-01-00:00_2022-12-01-23:59_Sentinel-2_L2A_True_color.jpg");
                     display3D(datatiff,indices,img);
//                     LayerRaster lr(layer.getPath(), layer.getName());
//                     std::cout<<layer.getName();
//                     std::vector<glm::vec3> datatiff = lr.dataTIFF;

//                     display(datatiff,layer.getPath());
                  }
                 else if(layer.format==Format::WFS)
                 {
                    LayerFeatures geoType= project->layerFeatures[keys.at(i)];
                    display(geoType);
                    //std::cout<<"WFS";
                 }
                 else if(layer.format==Format::WMS)
                 {
                     std::vector<glm::vec3> datatiff= project->layerRastersData[keys.at(i)];
                     QImage img= project->layerRastersDataQimage[keys.at(i)];
                     std::vector<glm::vec2> indices = project->layerRastersIndices[keys.at(i)];
                     //QImage img = QImage("/home/fernando/Downloads/EO_Browser_images/2022-12-01-00:00_2022-12-01-23:59_Sentinel-2_L2A_True_color.jpg");
                     display2D(datatiff,indices,img);

                  }


              }
         }
    }



//    glBegin(GL_TRIANGLES);
//          glNormal3f(0,-1,0.707);
//          glVertex3f(-1.0f,-1.0f,-1.0f);
//          glVertex3f(-1.0f,-1.0f, 1.0f);
//          glVertex3f(-1.0f, 1.0f, 1.0f);
//      glEnd();
//      glBegin(GL_TRIANGLES);
//          glNormal3f(0,-1,0.707);
//          glVertex3f(1.0f, 1.0f,-1.0f);
//          glVertex3f(-1.0f,-1.0f,-1.0f);
//          glVertex3f(-1.0f, 1.0f,-1.0f);
//      glEnd();
//      glBegin(GL_TRIANGLES);
//          glNormal3f(0,-1,0.707);
//          glVertex3f(1.0f,-1.0f, 1.0f);
//          glVertex3f(-1.0f,-1.0f,-1.0f);
//          glVertex3f(1.0f,-1.0f,-1.0f);
//    glEnd();

//    glBegin(GL_TRIANGLES);
//        glVertex3f(1.0f, 1.0f,-1.0f);
//        glVertex3f(1.0f,-1.0f,-1.0f);
//        glVertex3f(-1.0f,-1.0f,-1.0f);
//    glEnd();


    //Surface d'affichage raster de Fernando (CHAT)
//    glEnable(GL_TEXTURE_2D);
//    glBegin (GL_QUADS);

//    glTexCoord2f (0.0, 0.0);
//    glVertex3f(-1,-1,0);

//    glTexCoord2f (0.0, 1.0);
//    glVertex3f(-1,1,0);

//    glTexCoord2f (1.0, 1.0);
//    glVertex3f(1,1,0);

//    glTexCoord2f (1.0, 0.0);
//    glVertex3f(1,-1,0);
//    glEnd ();
//    glDisable(GL_TEXTURE_2D);
    //FIN DU CHAT

//    Point et ligne
//    glBegin(GL_POINTS);
//        glVertex3f(4,-1,0);
//    glEnd();
//    glBegin(GL_LINE_STRIP);
//        glVertex3f(5,0,0);
//        glVertex3f(5,1,0);
//        glVertex3f(6,0,0);
//    glEnd();

  /*  //DEBUT DU CUBE
    glBegin(GL_QUADS);
        glNormal3f(0,0,-1);
        glVertex3f(-1,-1,0);
        glVertex3f(-1,1,0);
        glVertex3f(1,1,0);
        glVertex3f(1,-1,0);

    glEnd();

    glBegin(GL_TRIANGLES);
          glNormal3f(0,-1,0.707);
          glVertex3f(-1.0f,-1.0f,-1.0f);
          glVertex3f(-1.0f,-1.0f, 1.0f);
          glVertex3f(-1.0f, 1.0f, 1.0f);
      glEnd();
      glBegin(GL_TRIANGLES);
          glNormal3f(0,-1,0.707);
          glVertex3f(1.0f, 1.0f,-1.0f);
          glVertex3f(-1.0f,-1.0f,-1.0f);
          glVertex3f(-1.0f, 1.0f,-1.0f);
      glEnd();
      glBegin(GL_TRIANGLES);
          glNormal3f(0,-1,0.707);
          glVertex3f(1.0f,-1.0f, 1.0f);
          glVertex3f(-1.0f,-1.0f,-1.0f);
          glVertex3f(1.0f,-1.0f,-1.0f);
    glEnd();

    glBegin(GL_TRIANGLES);
        glVertex3f(1.0f, 1.0f,-1.0f);
        glVertex3f(1.0f,-1.0f,-1.0f);
        glVertex3f(-1.0f,-1.0f,-1.0f);
    glEnd();
    glBegin(GL_TRIANGLES);
    glNormal3f(0,-1,0.707);
            glVertex3f(-1.0f,-1.0f,-1.0f);
            glVertex3f(-1.0f, 1.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f,-1.0f);
    glEnd();
    glBegin(GL_TRIANGLES);
    glNormal3f(0,-1,0.707);
           glVertex3f(1.0f,-1.0f, 1.0f);
           glVertex3f(-1.0f,-1.0f, 1.0f);
           glVertex3f(-1.0f,-1.0f,-1.0f);
       glEnd();
       glBegin(GL_TRIANGLES);
           glNormal3f(0,-1,0.707);
           glVertex3f(-1.0f, 1.0f, 1.0f);
           glVertex3f(-1.0f,-1.0f, 1.0f);
           glVertex3f(1.0f,-1.0f, 1.0f);

    glEnd();
    glBegin(GL_TRIANGLES);

    glNormal3f(0,-1,0.707);
            glVertex3f(1.0f, 1.0f, 1.0f);
            glVertex3f(1.0f,-1.0f,-1.0f);
            glVertex3f(1.0f, 1.0f,-1.0f);
        glEnd();
        glBegin(GL_TRIANGLES);
            glNormal3f(0,-1,0.707);
            glVertex3f(1.0f,-1.0f,-1.0f);
            glVertex3f(1.0f, 1.0f, 1.0f);
            glVertex3f(1.0f,-1.0f, 1.0f);
        glEnd();
        glBegin(GL_TRIANGLES);
            glNormal3f(0,-1,0.707);
            glVertex3f(1.0f, 1.0f, 1.0f);
            glVertex3f(1.0f, 1.0f,-1.0f);
            glVertex3f(-1.0f, 1.0f,-1.0f);
        glEnd();
        glBegin(GL_TRIANGLES);
            glNormal3f(0,-1,0.707);
            glVertex3f(1.0f, 1.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f,-1.0f);
            glVertex3f(-1.0f, 1.0f, 1.0f);
        glEnd();
        glBegin(GL_TRIANGLES);
            glNormal3f(0,-1,0.707);
            glVertex3f(1.0f, 1.0f, 1.0f);
            glVertex3f(-1.0f, 1.0f, 1.0f);
            glVertex3f(1.0f,-1.0f, 1.0f);
        glEnd();
        //FIN DU CUBE */

}


bool DisplayPanel::selected_object(int mouseX, int mouseY, int screenWidth, int screenHeight, glm::vec3 cam_pos, glm::vec3 cam_rot, std::vector<uint>* out_selected_feature_ID){
    //bool returned to tell if an object is selected or not
    bool res = false;

    glm::vec3 cam_dir = glm::normalize(glm::vec3(cos(cam_rot[1])*sin(cam_rot[0]), sin(cam_rot[1]), cos(cam_rot[0])*cos(cam_rot[1])));
    glm::vec3 r0 = glm::normalize(glm::vec3(sin(cam_rot[0]-3*M_PI/2), 0, cos(cam_rot[0]-3*M_PI/2)));
    glm::vec3 d0 = glm::normalize(glm::cross(cam_dir, r0));
    glm::vec3 right = glm::normalize(cos(cam_rot[2])*r0 + sin(cam_rot[2])*d0);
    glm::vec3 down = glm::normalize(glm::cross(cam_dir, right));
    float w = screenWidth;
    float h = screenHeight;

    //Je créé le rayon en supposant qu'on a une vue ortho
    glm::vec3 ray_origin = cam_pos + (mouseX - w/2)*right + (mouseY - h/2)*down;
    glm::vec3 ray_direction = cam_dir;

    //Dans le commentaire suivant, je créé le rayon en supposant qu'on a une focale de 1
    //glm::vec3 ray_origin = cam_pos;
    //glm::vec3 ray_direction = glm::normalize(cam_dir + (mouseX - w/2)*right + (mouseY - h/2)*down);

    //ScreenPosToWorldRay(mouseX, mouseY, screenWidth, screenHeight, viewMatrix, projMatrix, ray_origin, ray_direction);


    float dist_min = std::numeric_limits<float>::max(); //valeur max d'un float

    map<uint, LayerFeatures> mLayers = project->layerFeatures;
    for(std::map<uint, LayerFeatures>::iterator it = mLayers.begin(); it != mLayers.end(); it++) {
        vector<Feature> features = it->second.features;
        for(uint i_feat=0; i_feat<features.size(); i_feat++) {
            std::vector<glm::vec3> geom = features[i_feat].geometries;
            //On créé la bounding box de l'objet
            glm::vec3 bb_min(0,0,0);
            glm::vec3 bb_max(0,0,0);
            bool pass_elt = false;
            switch(features[i_feat].type){
                case Type::POINT:
                    bb_min = {geom[0][0] -0.5, geom[0][1] -0.5, geom[0][2] -0.5};
                    bb_max = {geom[0][0] +0.5, geom[0][1] +0.5, geom[0][2] +0.5};
                break;
                case Type::LINE:
                    bb_min = {geom[0][0], geom[0][1], geom[0][2]};
                    bb_max = {geom[0][0], geom[0][1], geom[0][2]};
                    for(int i_fline=1; i_fline<geom.size(); i_fline++){
                        for(int i=0; i<3; i++){
                            if(geom[i_fline][i] < bb_min[i]){
                                bb_min[i] = geom[i_fline][i];
                            }
                            if(geom[i_fline][i] > bb_max[i]){
                                bb_max[i] = geom[i_fline][i];
                            }
                        }
                    }
                    for(int i=0; i<3; i++){
                        if(bb_max[i] - bb_min[i] ==0){
                            bb_max[i] += 0.5;
                            bb_min[i] -= 0.5;
                        }
                    }
                break;
                case Type::POLYGON:
                    bb_min = {geom[0][0], geom[0][1], geom[0][2]};
                    bb_max = {geom[0][0], geom[0][1], geom[0][2]};
                    for(int i_fline=1; i_fline<geom.size(); i_fline++){
                        for(int i=0; i<3; i++){
                            if(geom[i_fline][i] < bb_min[i]){
                                bb_min[i] = geom[i_fline][i];
                            }
                            if(geom[i_fline][i] > bb_max[i]){
                                bb_max[i] = geom[i_fline][i];
                            }
                        }
                    }
                    for(int i=0; i<3; i++){
                        if(bb_max[i] - bb_min[i] ==0){
                            bb_max[i] += 0.5;
                            bb_min[i] -= 0.5;
                        }
                    }
                break;
                case Type::NULLTYPE:
                    pass_elt = true;
                break;
            }
            if(pass_elt){
                continue;
            }

            //Ici, on a les coordonnées de la bounding box de l'objet (bb_min et bb_max)
            float dist = 0;
            if ( rayBBIntersect(ray_origin, ray_direction, bb_min, bb_max, dist)/*TestRayOBBIntersection(ray_origin, ray_direction, bb_min, bb_max, glm::mat4(1), dist)*/) {
                cout<<"Touché !"<<endl;
                if(dist < dist_min){
                    dist_min = dist;
                    *out_selected_feature_ID = {it->first, i_feat};
                    res = true;
                }
            }

        }
    }
    return res;
}

bool rayBBIntersect(glm::vec3 ray_origin, glm::vec3 ray_direction, glm::vec3 bb_min, glm::vec3 bb_max, float& dist){
    glm::vec3 p(0, 0, 0); //point d'intersection entre rayon et plan
    float t(0);
    glm::vec3 centre = bb_min + 0.5f*(bb_max-bb_min);
    dist = sqrt(pow(centre[0]-ray_origin[0], 2) + pow(centre[1]-ray_origin[1], 2) + pow(centre[2]-ray_origin[2], 2));

    //1: eq de plan x=x_min
    if(ray_direction[0] != 0){
        t = (bb_min[0] - ray_origin[0])/ray_direction[0];
        p = glm::vec3(ray_origin[0]+t*ray_direction[0], ray_origin[1]+t*ray_direction[1], ray_origin[2]+t*ray_direction[2]);
        //On regarde si le point sur le plan est dans notre coupure du plan
        if(p[1]>bb_min[1] && p[1]<bb_max[1] && p[2]>bb_min[2] && p[2]<bb_max[2]){
            return true;
        }
    }

    //2: eq de plan x=x_max
    if(ray_direction[0] != 0){
        t = (bb_max[0] - ray_origin[0])/ray_direction[0];
        p = glm::vec3(ray_origin[0]+t*ray_direction[0], ray_origin[1]+t*ray_direction[1], ray_origin[2]+t*ray_direction[2]);
        //On regarde si le point sur le plan est dans notre coupure du plan
        if(p[1]>bb_min[1] && p[1]<bb_max[1] && p[2]>bb_min[2] && p[2]<bb_max[2]){
            return true;
        }
    }

    //3: eq de plan y=y_min
    if(ray_direction[1] != 0){
        t = (bb_min[1] - ray_origin[1])/ray_direction[1];
        p = glm::vec3(ray_origin[0]+t*ray_direction[0], ray_origin[1]+t*ray_direction[1], ray_origin[2]+t*ray_direction[2]);
        //On regarde si le point sur le plan est dans notre coupure du plan
        if(p[0]>bb_min[0] && p[0]<bb_max[0] && p[2]>bb_min[2] && p[2]<bb_max[2]){
            return true;
        }
    }

    //4: eq de plan y=y_max
    if(ray_direction[1] != 0){
        t = (bb_max[1] - ray_origin[1])/ray_direction[1];
        p = glm::vec3(ray_origin[0]+t*ray_direction[0], ray_origin[1]+t*ray_direction[1], ray_origin[2]+t*ray_direction[2]);
        //On regarde si le point sur le plan est dans notre coupure du plan
        if(p[0]>bb_min[0] && p[0]<bb_max[0] && p[2]>bb_min[2] && p[2]<bb_max[2]){
            return true;
        }
    }

    //5: eq de plan z=z_min
    if(ray_direction[2] != 0){
        t = (bb_min[2] - ray_origin[2])/ray_direction[2];
        p = glm::vec3(ray_origin[0]+t*ray_direction[0], ray_origin[1]+t*ray_direction[1], ray_origin[2]+t*ray_direction[2]);
        //On regarde si le point sur le plan est dans notre coupure du plan
        if(p[0]>bb_min[0] && p[0]<bb_max[0] && p[1]>bb_min[1] && p[1]<bb_max[1]){
            return true;
        }
    }

    //6: eq de plan z=z_max
    if(ray_direction[2] != 0){
        t = (bb_max[2] - ray_origin[2])/ray_direction[2];
        p = glm::vec3(ray_origin[0]+t*ray_direction[0], ray_origin[1]+t*ray_direction[1], ray_origin[2]+t*ray_direction[2]);
        //On regarde si le point sur le plan est dans notre coupure du plan
        if(p[0]>bb_min[0] && p[0]<bb_max[0] && p[1]>bb_min[1] && p[1]<bb_max[1]){
            return true;
        }
    }
    return false;
}


//https://github.com/opengl-tutorials/ogl/blob/master/misc05_picking/misc05_picking_custom.cpp
void ScreenPosToWorldRay(
    int mouseX, int mouseY,             // Mouse position, in pixels, from bottom-left corner of the window
    int screenWidth, int screenHeight,  // Window size, in pixels
    glm::mat4 ViewMatrix,               // Camera position and orientation
    glm::mat4 ProjectionMatrix,         // Camera parameters (ratio, field of view, near and far planes)
    glm::vec3& out_origin,              // Ouput : Origin of the ray. /!\ Starts at the near plane, so if you want the ray to start at the camera's position instead, ignore this.
    glm::vec3& out_direction            // Ouput : Direction, in world space, of the ray that goes "through" the mouse.
){

    // The ray Start and End positions, in Normalized Device Coordinates (modified to fit our viewport)
    float side = (float) min(screenWidth, screenHeight);
    glm::vec4 lRayStart_NDC(
        (2.0f*(float)mouseX - (float)screenWidth)/side, // [0,1024] -> [-1,1]
        (2.0f*(float)mouseY - (float)screenHeight)/side, // [0, 768] -> [-1,1]
        -1.0, // The near plane maps to Z=-1 in Normalized Device Coordinates
        1.0f
    );
    glm::vec4 lRayEnd_NDC(
        (2.0f*(float)mouseX - (float)screenWidth)/side,
        (2.0f*(float)mouseY - (float)screenHeight)/side,
        0.0,
        1.0f
    );


    // The Projection matrix goes from Camera Space to NDC.
    // So inverse(ProjectionMatrix) goes from NDC to Camera Space.
    glm::mat4 InverseProjectionMatrix = glm::inverse(ProjectionMatrix);

    // The View Matrix goes from World Space to Camera Space.
    // So inverse(ViewMatrix) goes from Camera Space to World Space.
    glm::mat4 InverseViewMatrix = glm::inverse(ViewMatrix);

    glm::vec4 lRayStart_camera = InverseProjectionMatrix * lRayStart_NDC;    lRayStart_camera/=lRayStart_camera.w;
    glm::vec4 lRayStart_world  = InverseViewMatrix       * lRayStart_camera; lRayStart_world /=lRayStart_world .w;
    glm::vec4 lRayEnd_camera   = InverseProjectionMatrix * lRayEnd_NDC;      lRayEnd_camera  /=lRayEnd_camera  .w;
    glm::vec4 lRayEnd_world    = InverseViewMatrix       * lRayEnd_camera;   lRayEnd_world   /=lRayEnd_world   .w;


    // Faster way (just one inverse)
    //glm::mat4 M = glm::inverse(ProjectionMatrix * ViewMatrix);
    //glm::vec4 lRayStart_world = M * lRayStart_NDC; lRayStart_world/=lRayStart_world.w;
    //glm::vec4 lRayEnd_world   = M * lRayEnd_NDC  ; lRayEnd_world  /=lRayEnd_world.w;


    glm::vec3 lRayDir_world(lRayEnd_world - lRayStart_world);
    lRayDir_world = glm::normalize(lRayDir_world);


    out_origin = glm::vec3(lRayStart_world);
    out_direction = glm::normalize(lRayDir_world);
}

//https://github.com/opengl-tutorials/ogl/blob/master/misc05_picking/misc05_picking_custom.cpp
bool TestRayOBBIntersection(
    glm::vec3 ray_origin,        // Ray origin, in world space
    glm::vec3 ray_direction,     // Ray direction (NOT target position!), in world space. Must be normalize()'d.
    glm::vec3 aabb_min,          // Minimum X,Y,Z coords of the mesh when not transformed at all.
    glm::vec3 aabb_max,          // Maximum X,Y,Z coords. Often aabb_min*-1 if your mesh is centered, but it's not always the case.
    glm::mat4 ModelMatrix,       // Transformation applied to the mesh (which will thus be also applied to its bounding box)
    float& intersection_distance // Output : distance between ray_origin and the intersection with the OBB
){

    // Intersection method from Real-Time Rendering and Essential Mathematics for Games

    float tMin = 0.0f;
    float tMax = 100000.0f;

    glm::vec3 OBBposition_worldspace(ModelMatrix[3].x, ModelMatrix[3].y, ModelMatrix[3].z);

    glm::vec3 delta = OBBposition_worldspace - ray_origin;

    // Test intersection with the 2 planes perpendicular to the OBB's X axis
    {
        glm::vec3 xaxis(ModelMatrix[0].x, ModelMatrix[0].y, ModelMatrix[0].z);
        float e = glm::dot(xaxis, delta);
        float f = glm::dot(ray_direction, xaxis);

        if ( fabs(f) > 0.001f ){ // Standard case

            float t1 = (e+aabb_min.x)/f; // Intersection with the "left" plane
            float t2 = (e+aabb_max.x)/f; // Intersection with the "right" plane
            // t1 and t2 now contain distances betwen ray origin and ray-plane intersections

            // We want t1 to represent the nearest intersection,
            // so if it's not the case, invert t1 and t2
            if (t1>t2){
                float w=t1;t1=t2;t2=w; // swap t1 and t2
            }

            // tMax is the nearest "far" intersection (amongst the X,Y and Z planes pairs)
            if ( t2 < tMax )
                tMax = t2;
            // tMin is the farthest "near" intersection (amongst the X,Y and Z planes pairs)
            if ( t1 > tMin )
                tMin = t1;

            // And here's the trick :
            // If "far" is closer than "near", then there is NO intersection.
            // See the images in the tutorials for the visual explanation.
            if (tMax < tMin )
                return false;

        }else{ // Rare case : the ray is almost parallel to the planes, so they don't have any "intersection"
            if(-e+aabb_min.x > 0.0f || -e+aabb_max.x < 0.0f)
                return false;
        }
    }


    // Test intersection with the 2 planes perpendicular to the OBB's Y axis
    // Exactly the same thing than above.
    {
        glm::vec3 yaxis(ModelMatrix[1].x, ModelMatrix[1].y, ModelMatrix[1].z);
        float e = glm::dot(yaxis, delta);
        float f = glm::dot(ray_direction, yaxis);

        if ( fabs(f) > 0.001f ){

            float t1 = (e+aabb_min.y)/f;
            float t2 = (e+aabb_max.y)/f;

            if (t1>t2){float w=t1;t1=t2;t2=w;}

            if ( t2 < tMax )
                tMax = t2;
            if ( t1 > tMin )
                tMin = t1;
            if (tMin > tMax)
                return false;

        }else{
            if(-e+aabb_min.y > 0.0f || -e+aabb_max.y < 0.0f)
                return false;
        }
    }


    // Test intersection with the 2 planes perpendicular to the OBB's Z axis
    // Exactly the same thing than above.
    {
        glm::vec3 zaxis(ModelMatrix[2].x, ModelMatrix[2].y, ModelMatrix[2].z);
        float e = glm::dot(zaxis, delta);
        float f = glm::dot(ray_direction, zaxis);

        if ( fabs(f) > 0.001f ){

            float t1 = (e+aabb_min.z)/f;
            float t2 = (e+aabb_max.z)/f;

            if (t1>t2){float w=t1;t1=t2;t2=w;}

            if ( t2 < tMax )
                tMax = t2;
            if ( t1 > tMin )
                tMin = t1;
            if (tMin > tMax)
                return false;

        }else{
            if(-e+aabb_min.z > 0.0f || -e+aabb_max.z < 0.0f)
                return false;
        }
    }

    intersection_distance = tMin;
    return true;

}

 std::vector<std::vector<string>> DisplayPanel::getObjectClickedData(){
     getinfos.clear();
    int layerKey = Object_feat_ID.at(0);
    int ObjectKey = Object_feat_ID.at(1);
    std::string nameLayerObject = project->LayerByID(layerKey)->getName();
    std::string pathObjectLayer = project->LayerByID(layerKey)->getPath();
    LayerVector layer(pathObjectLayer,nameLayerObject);


   for(int j = 0;j<layer.getAttributes().at(ObjectKey).size();j++){
       getinfos.push_back({layer.getFieldNames().at(j),layer.getAttributes().at(ObjectKey).at(j)});
   }

    return getinfos;
}
