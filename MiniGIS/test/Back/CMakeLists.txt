cmake_minimum_required(VERSION 3.5)

project(Back LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 Qt5 COMPONENTS Widgets REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Widgets REQUIRED)

add_executable(Back main.cpp
    ../../src/Back/ogrlayer.cpp
    ../../src/Back/layervector.cpp
    ../../src/Back/layerraster.cpp
    ../../src/Back/project.cpp
    ../../src/Back/layer.cpp
    ../../src/Back/wfs.cpp
    ../../src/Back/vectorGeometry.cpp
    ../../src/Back/layerGML.cpp
    ../../src/Back/wms.cpp
    ../../src/Back/layerrgb.cpp
    ../../src/Back/layermnt.cpp

    ../../include/Back/ogrlayer.h
    ../../include/Back/layervector.h
    ../../include/Back/layerraster.h
    ../../include/Back/project.h
    ../../include/Back/layer.h
    ../../include/Back/wfs.h
    ../../include/Back/vectorGeometry.h
    ../../include/Back/layerGML.h
    ../../include/Back/wms.h
    ../../include/Back/layerrgb.h
    ../../include/Back/layermnt.h

)


# find system installed GDAL package with predefined CMake variable for finding GDAL
find_package(GDAL REQUIRED)

# Specify location of GDAL header files
include_directories( include ${GDAL_INCLUDE_DIRS} usr/include/jsoncpp)

# Specify GDAL libraries to link your cpp executable target against
target_link_libraries( Back ${GDAL_LIBRARIES} Qt${QT_VERSION_MAJOR}::Widgets jsoncpp libcitygml.so)

