#ifndef INFOSFEATURESPANEL_H
#define INFOSFEATURESPANEL_H
#include <list>
#include "Back/project.h"
//Inclure le type Feature

class InfosFeaturesPanel {
    public:
        /**
         * @brief Construct a new InfosFeaturesPanel object
         *
         */
        InfosFeaturesPanel();
        /**
         * @brief Destroy a InfosFeaturesPanel object
         *
         */
        //~InfosFeaturesPanel();

    public :
        /**
         * @brief Add a feature to the features list
         *
         * @param feature An object of Feature type
         */
        //void addFeature(Feature feature);

        /**
         * @brief Remove a feature from the features list
         *
         * @param feature An object of Feature type
         */
        //void removeFeature(Feature feature);
        std::vector<Layer> getvisiblelayer(Project *project);
        std::vector<Layer> visibleinfo;

    private:
        /**
         * @brief The features list
         *
         */
        //std::list<Feature> features;
};

#endif // INFOSFEATURESPANEL_H
