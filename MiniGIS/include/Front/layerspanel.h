#ifndef LAYERSPANEL_H
#define LAYERSPANEL_H

#include <iostream>
#include <vector>
#include <QtWidgets>
#include <QtOpenGL>
#include <QOpenGLWidget>
#include <GL/glu.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <string>
#include <QInputDialog>
#include <QFileDialog>
#include <QCheckBox>
#include "mainwindow.h"
#include "include/Back/layer.h"
#include "../Back/project.h"
#include "../../include/Back/layervector.h"
#include "../../include/Back/wfs.h"

class LayersPanel {

public:
    LayersPanel();

    /**
     * @brief Enables to add a Vector file to the project. The user has to search a file
     * on his computer. It can only be a Shapefile (SHP).
     * @param parent (MainWindow). It helps to call the function in the MainWindow.cpp.
     * @param project It's the project in which the user is working in (auto-selected) by RuGIS.
     * @see addLayerSHP(path, name)
     * @return std::vector<QString> (vector/list of strings/text) with :
     * - the filepath
     * - or the filename + the id of the layer from the project
     * - or an empty filepath
     */
    std::vector<QString> addVector(QWidget *parent, Project* project);

    /**
     * @brief Enables to add a Raster file to the project. The user has to search a file
     * on his computer. It can only be a TIF, TIFF or GEOTIFF file.
     * @param parent (MainWindow). It helps to call the function in the MainWindow.cpp.
     * @param project It's the project in which the user is working in (auto-selected) by RuGIS.
     * @see addLayerTIFF(path, name)
     * @return std::vector<QString> (vector/list of strings/text) with :
     * - the filepath
     * - or the filename + the id of the layer from the project
     * - or an empty filepath
     */
    std::vector<QString> addRaster(QWidget *parent, Project* project);

    std::vector<QString> addMNT(QWidget *parent, Project *project);


    /**
     * @brief Enables to connect to a WFS flow. The user has to give the link to the WFS flow
     * and select the name of the layer he wants to add to the project.
     * @param parent (MainWindow). It helps to call the function in the MainWindow.cpp.
     * @param project It's the project in which the user is working in (auto-selected) by RuGIS.
     * @return std::vector<QString> (vector/list of strings/text) with :
     * - the filepath
     * - or the filename + the id of the layer from the project
     * - or an empty filepath
     * - or a filepath + an empty filename
     */
    std::vector<QString> addWFS(QWidget *parent,Project *project);

    /**
     * @brief Enables to connect to a WMS flow. The user has to give the link to the WMS flow
     * and has to name the layer which will be added to the project.
     * @param parent (MainWindow). It helps to call the function in the MainWindow.cpp.
     * @param project It's the project in which the user is working in (auto-selected) by RuGIS.
     * @return std::vector<QString> (vector/list of strings/text) with :
     * - or the filename + the id of the layer from the project
     * - or an empty filepath (or empty string)
     * - or a filepath + an empty filename
     */
    std::vector<QString> addWMS(QWidget *parent,Project *project);


    /**
     * @brief Enables to add a CityGML file to the project. The user has to search
     * a file on his computer. It can only be a CityGML (GML) file.
     * @param parent (MainWindow). It helps to call the function in the MainWindow.cpp.
     * @param project It's the project in which the user is working in (auto-selected) by RuGIS.
     * @see addLayerGML(path, name)
     * @return std::vector<QString> (vector/list of strings/text) with :
     * - or the filename + the id of the layer from the project
     * - or an empty filepath (or empty string)
     */
    std::vector<QString> addCityGML(QWidget *parent,Project *project);



    // Container qui reçoit les Checkbox des couches
    QVBoxLayout* container;
};
/**
 * @brief display(LayerFeatures &vector). Is used to display a LayerFeature (SHP, GML, WFS) in The OpenGL panel.
 * @param vector The layer is a visisble layer of the project and a list of features
 * with a format associated to them. It loops on each feature and it will call the
 * display(Feature feat) function.
 * @see display(Feature feat)
 */
void display(LayerFeatures &vector);

/**
 * @brief display(Feature feat). Is used to display a Feature. It will test if it's a POLYGON, a POINT
 * or a LINESTRING and display it accordingly.
 * @param feat Is made up of a geometry and a type.
 */
void display(Feature feat);
void display(LayerFeatures &vector);
void display2D(std::vector<glm::vec3> data,std::vector<glm::vec2> indices, QImage tiffimage );
void display3D(std::vector<glm::vec3> data,std::vector<glm::vec2> indices, QImage tiffimage );


#endif // LAYERSPANEL_H
