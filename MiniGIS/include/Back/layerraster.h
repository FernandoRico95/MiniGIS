#ifndef LAYERRASTER_H
#define LAYERRASTER_H

#include "glm/glm.hpp"
#include "gdal_priv.h"
#include <vector>
#include "../../include/Back/layer.h"
#include <uchar.h>
#include <QImage>


/**
 * !brief LayerRaster is a Layer based on Raster data,
 * It is composed of Rasters with numerous bands
 */
class LayerRaster  : public Layer
{
public:

    /**
     * @brief Constructor of LayerRaster Class.
     *
     * @param path Path to GeoTIFF file
     * @param LayerName Name of GeoTIFF Layer.
     *
     * @return WMS Object
     *
     */
    LayerRaster(std::string path, std::string layerName);

    /**
     * @brief Opens the GeoTIFF Data
     *
     * @param path Path to GeoTIFF file
     *
     */
    void openGeotiff(std::string path);

    /**
     * @brief Computes the size of the GeoTIFF Data.
     *
     * @return Raster size and band count
     *
     */
    int* getGeotiffSize();

    std::vector<unsigned int> indexTable; // data that will be put in the indexBuffer
    std::vector<std::vector<float>> rawData;
    void fetchRaster(std::string path);
    void createDataCoordinates();
    void createTexture(std::string path);


    /**
     * @brief data that will be put in the vertexBuffer.
     *
     */
    std::vector<glm::vec3> dataTIFF;

    /**
     * @brief Width and height of the raster Data, and its number of bands.
     *
     */
    int widthTIFF, heightTIFF, numberOfBands;

    /**
     * @brief Origins and Pixel sizes of the raster data.
     *
     */
    float Xo, Yo, Xsize, Ysize;

    /**
     * @brief Computes the data that will be inserted into the VertexBuffer.
     *
     * @param path Path to GeoTIFF file.
     *
     */
    void fetchGeotiff(std::string path);

    /**
     * @brief Computes indices of the raster image.
     *
     * @param path Path to GeoTIFF file.
     *
     */

    void createIndexTable(std::string path);

    /**
     * @brief Returns the size of a pixel of the image.
     *
     * @return size of a pixel.
     *
     */
    float* getGeotiffOrigin();

    /**
     * @brief Returns the size of a pixel of the image.
     *
     * @return size of a pixel.
     *
     */
    float* getPixelSize();

    /**
     * @brief Returns the projection in which the data was given.
     *
     * @return projection of GeoTIFF.
     *
     */
    const char* getGeotiffProjection();

    /**
     * @brief Bounding Box of raster layer.
     *
     * @return <Xmin, Xmax, Ymin, Ymax>
     *
     */
    vector<float> getRasterBox();

    /**
     * @brief The image that has been processed.
     *
     */
    QImage qtimage;

    /**
     * @brief list of indices in the raster image.
     *
     */
    std::vector<glm::vec2> indices;

private:

    /**
     * @brief Path to GeoTIFF file.
     *
     */
    std::string path;

    /**
     * @brief Name of WMS Layer.
     *
     */
    std::string layerName;

    /**
     * @brief Dataset used by OGR and GDAL.
     *
     */
    GDALDataset *geotiffData;
};

#endif // LAYERRASTER_H
