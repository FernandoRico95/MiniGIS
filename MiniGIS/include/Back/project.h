#ifndef PROJECT_H
#define PROJECT_H

#include <vector>
#include <iostream>
#include <fstream>

#include <map>
#include <algorithm>
#include "../../include/Back/layervector.h"
#include "../../include/Back/vectorGeometry.h"
#include "../../include/Back/layerGML.h"

#include <cstdio>

#include <jsoncpp/json/value.h>
#include <jsoncpp/json/json.h>

#include <cstring>
#include "../../include/Back/wfs.h"

#include <qimage.h>


using namespace std;

class Project
{

private:
    unsigned int projection;
    map<uint , Layer> layers;
    std::string path;
    std::string name;
    int layerNombre;

    // USEFUL METHODS FOR ID

        uint counter(int layerNombre);

public:
// CONSTRUCTORS
    Project();
    Project(std::string path);
    //Project(const char*  path);

// DESTRUCTOR
    ~Project();


//    SHP
    uint addLayerSHP(std::string &path, std::string &layerName);
    uint addLayerWFS(std::string &path, std::string &layerName);
//    GML
    uint addLayerGML(std::string &path, std::string &layerName, int srs = 3946);
//TIFF
    uint addLayerTIFF(std::string &path, std::string &layerName);
    uint addLayerMNT(std::string &path, std::string &pathTexture, std::string &layerName);
    uint addLayerWMS(std::string &path, std::string &layerName);



// TIFF
//    uint addLayerTIFF(std::string &path, std::string &layerName);

// GETTERS
//    Feature getGeoms(uint idLayer);

    map<uint, Layer> * getLayers();
    std::string getPath(){ return path; }
    unsigned int getProjection(){ return projection; }
    std::string getName(){ return name; }
    Layer* LayerByID(uint id);
    map<uint, Layer> getSelectedLayers();
    map<uint, Layer> getNonHiddenLayers();
    map<uint , LayerFeatures> layerFeatures;
    map<uint ,  std::vector<glm::vec3>> layerRastersData;
    map<uint , QImage > layerRastersDataQimage;
    map<uint , std::vector<glm::vec2> > layerRastersIndices;
    map<uint, std::vector<float>> layerRastersBBox;

    map<uint, Layer> * getShplayers();
    vector<uint> extract_keys();
    void getFieldsGeoms();
    void getFieldsNamesShp();

// SETTERS
    void setPath(std::string _path){
        path = _path;
        save(path);
    }
    void setProjection(unsigned int _projection){ projection = _projection; }
    void setName(std::string _name){ name = _name; }


// REMOVERS
    void removeLayer(uint id);
    void removeSelectedLayers();

// SAVERS
    void save();
    void save(std::string path);
    void load(std::string path);


};

#endif // PROJECT_H
