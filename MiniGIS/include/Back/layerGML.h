#ifndef LAYERGML_H
#define LAYERGML_H

#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

#include <citygml/citygml.h>
#include <citygml/object.h>
#include <citygml/citymodel.h>
#include <citygml/geometry.h>
#include <citygml/polygon.h>
#include <citygml/envelope.h>
#include <citygml/attributesmap.h>
#include <citygml/address.h>

#include "../../include/Back/layer.h"
#include "../../include/Back/vectorGeometry.h"


/**
 * !brief Layer GML is a Layer based on GML data,
 * A GML file is composed of objects (features), themselves are composed of
 */
class LayerGML : public Layer
{
private:
    /**
     * @brief layer feature object
     *
     */
    LayerFeatures layerFeatures;

    /**
     * @brief srs of the layer
     *
     */
    int srs;

    /**
     * @brief setter LayerFeature, permit to implement the features object of listFeature
     *
     */
    void setLayerFeature(std::vector<Feature> Listfeatures){
        this->layerFeatures.features = Listfeatures;
    }

    /**
     * @brief compute geometries of any feature in the layer, layerFeature attribute is implemented
     *
     */
    void computeGeometries();

    /**
     * @brief browsing cityGML object to extract every geometries
     *
     */
    std::vector<Feature> analyzeObject(const citygml::CityObject* object);


public:

    /**
     * @brief Construct a new my LayerGML object, qith inheritance from Layer
     *
     * @param path path toward the file
     * @param layerName name of the layer
     */
    LayerGML(std::string path, std::string layerName);

    /**
     * @brief getter layerFeature
     *
     * @return LayerFeatures
     */
    LayerFeatures getLayerFeatures(){return layerFeatures;}

    /**
     * @brief computing bounding box of the object
     *
     * @return vector<float> {min x,max x,min y, max y, min z, max z}
     */
    vector<float> getBoundingBox() ;

    /**
     * @brief getting attributes of the object
     */
    void getAttributes();

    /**
     * @brief getting srs of the file
     *
     * @return string
     */
    int getCurrentSRS() {return srs;}

    /**
     * @brief setting the current srs of the file
     *
     */
    void setCurrentSRS(int _currentsrs) {srs = _currentsrs;}


    /**
     * @brief changing srs of the file
     *
     */
    void changeSRS(int targetSRS);

};
#endif // LAYERGML_H
