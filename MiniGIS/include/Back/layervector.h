#ifndef LAYERVECTOR_H
#define LAYERVECTOR_H
#include <string>
#include <vector>
#include <glm/glm.hpp>

#include "../../include/Back/layer.h"
#include "../../include/Back/vectorGeometry.h"
//#include <ogrsf_frmts.h>
#include "glm/glm.hpp"



class LayerVector : public Layer
{
private:


    /**
     * @brief Datataset used by OGR and GDAL
     *
     */
    GDALDataset* dataset;


    /**
     * @brief layer feature object
     *
     */
    LayerFeatures layerFeatures;

public:

    /**
     * @brief Construct Layer Vector object, qith inheritance from Layer
     *
     * @param path path toward the file
     * @param layerName name of the layer
     */
    LayerVector(std::string path, std::string layerName );

    /**
     * @brief compute geometries of any feature in the layer, layerFeature attribute is implemented
     *
     */
    void  computeGeometries();

    /**
     * @brief getting field names of the object
     * @return vector<string> {fieldnames}
     */
    vector<string> getFieldNames();

    /**
     * @brief getting all the attributes of the object
     */
    vector<vector<string>> getAttributes();

    /**
     * @brief getting one specific values of the object
     * @param long IID corresponding to the desired id
     * @return vector<string> {values attributes}
     */
    vector<string> getAttributesID(long IID);

    /**
     * @brief getter layerFeature
     *
     * @return LayerFeatures
     */
    LayerFeatures getLayerFeatures(){return layerFeatures;}

    /**
     * @brief getting bounding box features of the object
     * @return vector<float> {MinX, MaxX, MinY, MaxY}
     */
    vector<float> getBoundingBoxFeature(uint id) ;

    /**
     * @brief getting bounding box layer of the object
     * @return vector<float> {MinX, MaxX, MinY, MaxY}
     */
    vector<float> getBoundingBoxLayer();

    /**
    * @brief setter for the GDALDataset
    * @param newDataset
    */
    void setLayerFeature(std::vector<Feature> Listfeatures){
        this->layerFeatures.features = Listfeatures;
    }


    /**
     * @brief vg
     *
     * @return vectorGeometry
     */
    VectorGeometry vg;

};
///home/formation/Documents/MiniGIS/MiniGIS/test/Back/data
#endif // LAYERVECTOR_H
