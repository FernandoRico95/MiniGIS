#ifndef LAYER_H
#define LAYER_H
#include <string>
#include <vector>
//#include "ogrsf_frmts.h"

using namespace std;

/**
 * !brief The aim of this class is to represent a layer with multiple features.
 */

enum class Format {
    SHP,
    TIFF,
    GML,
    WFS,
    WMS,
    MNT
};

class Layer
{
protected:

//    static uint currentID;
    /**
     * @brief Projection (SRC)
     */
    string projection;
    /**
     * @brief Determines whether the layer is visible
     */
    bool hidden;
    /**
     * @brief Determines whether the layer is selected
     */
    bool selected;
    /**
     * @brief Absolute path to local layer file
     */
    std::string path;

    /**
     * @brief name given to layer
     */
     std::string name;


public:
    /**
     * @brief Creates the layer object
     */
    Layer();
    Layer(std::string _path, std::string _layerName);
    uint getId(){return layerID;}
    /*
     * @brief object id
     */
    uint layerID ;
    /**
     * @brief format
     */

    Format format;

//    static uint ID(){static uint currentID =0; return currentID++;}
    string getProjection(){return this->projection;}
    void setProjection(std::string proj){this->projection = proj;}

    bool getHidden(){return hidden;}
    bool getSelected(){return selected;}
    std::string getPath(){return path;}
    std::string getName(){return name;}
    void setHidden(bool b){hidden = b;}
    void setSelected(bool b){selected = b;}
    void setPath(std::string _path){path=_path;}
    vector<float> getBoundingBoxFeature(uint index){return vector<float>{};}

//    void setName(const char* name);
//    void load();
//    void changeSelection();
//    void display();
//    void save();
};

#endif // LAYER_H
