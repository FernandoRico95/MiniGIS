#ifndef WFS_H
#define WFS_H

#include "gdal_priv.h"
#include <ogrsf_frmts.h>
#include <ogr_geometry.h>
#include "gdal/gdal.h"
#include "gdal/cpl_string.h"
#include "glm/glm.hpp"

#include <iostream>
#include <regex>
#include <string>
#include <vector>

#include "../../include/Back/vectorGeometry.h"
#include "../../include/Back/layer.h"


class Wfs: public Layer
{
private:
    GDALDataset* dataset;

    LayerFeatures layerFeatures;

    std::vector<std::string> layerNames;

public:

    /**
     * @brief Construct a new Wfs object
     *
     * @param name
     * @param path
     */
    Wfs(std::string path, std::string layerName);


    Wfs(std::string path);


    /**
     * @brief Destroy the Wfs object
     *
     */
    ~Wfs();


    /**
     * @brief connect to the WFS and initialize the dataset
     * @return dataset
     */
    GDALDataset* fetchWFS(std::string filePath);


    /**
     * @brief Get Projection of the shapefile
     * @return OGRSpatialReference
     */
    int GetProjection(std::string layerName);


    /**
     * @brief Generate the Vertexes of the shapefile data
     */
    void computeGeometries(std::vector<std::string> layerNames);


    /**
     * @brief setDatasetByName
     */
     void setDatasetSpatialRef();


    /**
     * @brief initializeVertex
     */
    void initializeVertex();


    /**
     * @brief dataset getter
     * @return dataset
     */
    GDALDataset* getDataset(){return this->dataset;};


    /**
    * @brief setter for the GDALDataset
    * @param newDataset
    */
    void setDataset(GDALDataset* dataset){this->dataset = dataset;};


    LayerFeatures getLayerFeatures(){return this->layerFeatures;}

    /**
    * @brief setter for the GDALDataset
    * @param newDataset
    */
    void setLayerFeature(std::vector<Feature> Listfeatures){
        this->layerFeatures.features = Listfeatures;
    }

    void foundLayerNames();

    std::vector<std::string> getLayerNames(){ return this->layerNames; };

    void setLayerNames(std::vector<std::string> layerNames){ this->layerNames = layerNames; };

    void pushLayerNames(std::string name){ return this->layerNames.push_back(name); };

    VectorGeometry vg;

    std::vector<std::string> getAttributesID(long IID);

    vector<float> getBoundingBoxFeature(uint index);

    vector<float> getBoundingBoxLayer();

};



#endif // Wfs_H
