#ifndef LAYERMNT_H
#define LAYERMNT_H

#include "glm/glm.hpp"
#include "gdal_priv.h"
#include <vector>
#include <iostream>
#include "../../include/Back/layer.h"
#include "../../include/Back/layerraster.h"
#include <uchar.h>
#include <QImage>

class LayerMNT  : public LayerRaster
{
public:
    LayerMNT(std::string path, std::string pathTexture, std::string layerName);
    void openGeotiff(std::string path); // file loader function
    int* getGeotiffSize();
    std::vector<unsigned int> indexTable; // data that will be put in the indexBuffer
    std::vector<glm::vec3> dataTIFF; // data that will be put in the vertexBuffer
    int widthTIFF, heightTIFF, numberOfBands; // number of raster bands
    std::vector<std::vector<float>> rawData;
    float Xo, Yo, Xsize, Ysize;
    void fetchRaster(std::string path);
    void createDataCoordinates();
    void createTexture(std::string path);
    void createIndexTable(std::string path);
    float* getGeotiffOrigin();
    float* getPixelSize();
    const char* getGeotiffProjection();
    QImage qtimage;
    std::vector<glm::vec2> indices;

private:
    std::string path;
    std::string pathTexture;
    std::string layerName;
    GDALDataset *geotiffData; // data in the GDALDataset format
};

#endif // LAYERMNT_H
